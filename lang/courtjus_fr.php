<?php
// This is a SPIP language file	 --	 Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'courtjus_titre' => 'Court-jus',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	// T
	'titre_page_configurer_courtjus' => 'Court-jus',
	'courtjus_objet_exclu' => 'Choisir les objets qui ne seront <strong>PAS</strong> court-cuircuités.',
	'explication_squelette_par_rubrique' => 'Rediriger si la rubrique possède un squelette spécifique ? (rubrique=2.html/rubrique-2.html)',
	'label_squelette_par_rubrique' => 'Squelette par rubrique',
	'label_num_titre' => 'Article numéroté',
	'explication_num_titre' => "Rediriger sur l'article avec le plus petit critère <em>num titre</em>",
	'chercher_rubrique_enfant' => 'Chercher dans les rubriques enfants ?',
	'explication_chercher_rubrique_enfant' => 'Permettre de chercher dans les sous-rubriques pour trouver un objet ?',
	'label_recent' => 'Article récent',
	'explication_recent' => 'Rediriger sur l\'article le plus récent ?',
	'label_mot_exclusion' => 'Ne pas rediriger si un de ces mots-clés est présent',
	'label_secteur_exclusion' => 'Ne pas rediriger si l\'objet se trouve dans un de ces secteurs'
);
