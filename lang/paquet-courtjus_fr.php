<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'courtjus_description' => 'Permet de rediriger certaines rubriques vers un élément particulier.

Clone du plugin \"Court-circuit\" qui fonctionne avec tout type d\'objet.',
	'courtjus_nom' => 'Court-jus',
	'courtjus_slogan' => 'Accès direct aux éléments des rubriques',
);

?>